function FunctionPreparator(evolutionStrategyAlgorithm) {

	var bestSolution;

	this.getBestSolution = function() {
		return this.bestSolution;
	}

	this.getPoints = function() {
		var bestSolution = evolutionStrategyAlgorithm.getBestSolution();

		this.bestSolution = bestSolution;
		
		var a = bestSolution[0];
		var b = bestSolution[1];
		var c = bestSolution[2];

		points = new Array();
		var xi = -5;
		for (var i=0; i<100; i++) {
			points.push({x: xi, y: evolutionStrategyAlgorithm.fTarget(a, b, c, xi)});

			xi += 0.1;
		}

		return points;
	}
}