function FunctionCanvasPainter() {
	var scaleX = 60;
	var scaleY = -4;
	var vectorX = 300;
	var vectorY = 550;

	this.paintFromPoints = function(ctx, lineColor, lineWidth, points) {
		ctx.beginPath();
		ctx.strokeStyle=lineColor;
		ctx.lineWidth=lineWidth;

		ctx.moveTo(vectorX + scaleX*points[0].x,vectorY +  scaleY*points[0].y);

		for (i = 1; i < points.length - 2; i ++)
		{
		  var xc = (points[i].x + points[i + 1].x) / 2;
		  var yc = (points[i].y + points[i + 1].y) / 2;
		  ctx.quadraticCurveTo(vectorX + scaleX*points[i].x, vectorY + scaleY*points[i].y,vectorX +  scaleX*xc,vectorY +  scaleY*yc);
		}
		 // curve through the last two points
		ctx.quadraticCurveTo(vectorX + scaleX*points[i].x,vectorY +  scaleY*points[i].y,vectorX +  scaleX*points[i+1].x, vectorY + scaleY*points[i+1].y);

		ctx.stroke();
	}
}