function EvolutionStrategyAlgorithm(points) {

	var GENE = 0;
	var STDEV = 1;
	var NUMBER_OF_GENES = 3;
	var POPULATION_NUMBER = 3;
	var EPS = 5;
	var INIT_BEST_QUALITY = 1000000000;

	var population = new Array();
	var nextGenearation = new Array();
	var means = Array();

	var numberOfIteration;

	this.process = function(populationNumber, accuracy) {
		POPULATION_NUMBER = populationNumber;

		this.initializePopulation();

		var bestQuality = INIT_BEST_QUALITY;
		for (var j=0; j < 1000 && bestQuality > accuracy; j++) {

			population = this.makeMutation(POPULATION_NUMBER, NUMBER_OF_GENES, this.calculateTauOne(), this.calculateTauTwo());			
			means = this.calculateMeans(POPULATION_NUMBER, points);

			bestQuality = this.findBestQuality();
			numberOfIteration = j;
		}
	}

	this.getNumberOfIterations = function() {
		return numberOfIteration;
	}

	this.updateWithMeans = function() {
		var finalPopulation = new Array();
		for (var i=0; i<population.length; i++)
			finalPopulation.push(population[means[i][1]]);

		population = finalPopulation;
	}

	this.getBestSolution = function() {
		return population[means[0][1]][GENE];	
	}

	this.getPopulation = function() {
		return population;
	}

	this.findBestQuality = function(menas) {
		return means[0][0];
	}

	this.calculateTauOne = function() {
		return 1.0/Math.sqrt(2.0 * POPULATION_NUMBER);
	}

	this.calculateTauTwo = function() {
		return 1.0/Math.sqrt(2.0 * Math.sqrt(POPULATION_NUMBER));
	}

	/*
	 * In matlab called randn()
	 */
	this.standardRandomGaussianDeviation = function() {
		var r, x, y;
	  
	  	// find a uniform random point (x, y) inside unit circle
	  	do {
	    	x = 2.0 * Math.random() - 1.0;
	     	y = 2.0 * Math.random() - 1.0;
	     	r = x*x + y*y;
	  	} while (r > 1 || r == 0);  // loop executed 4 / pi = 1.273.. times on average
	                                // http://en.wikipedia.org/wiki/Box-Muller_transform
	  	// apply the Box-Muller formula to get standard Gaussian z    
	  	return x * Math.sqrt(-2.0 * Math.log(r) / r);
	}

	this.fTarget = function(a, b, c, x) {
		return a * (Math.pow(x, 2.0) - b * Math.cos(c * Math.PI * x));
	}

	this.calculateMeans = function(populationNumber, points) {
		var means = new Array();

		for (var specimen=0; specimen < populationNumber; specimen++) { 
			var genes = population[specimen][GENE];
			
			var sum = 0;
			for (var i=0; i<populationNumber; i++)
			{
				var x = points[i].x;
				var y = points[i].y;
				var fVal = y - this.fTarget(genes[0], genes[1], genes[2], x);
				sum += Math.pow(fVal, 2.0);
			}

			var mean = sum/100;
			means.push([mean, specimen]);
		}

		means.sort();

		return means;
	}

	this.makeMutation = function(populationNumber, numberOfGenes, tau1, tau2) {
	
		var nextGeneration = new Array();

		for (var parent=0; parent<POPULATION_NUMBER; parent++) {
			expRandnTau1 = Math.exp(this.standardRandomGaussianDeviation() * tau1);

			var chromosom = Array();
			var genes = new Array();
			var stDev = new Array();
			
			for (var gene=0; gene<NUMBER_OF_GENES; gene++) {
				var actualGene = population[parent][GENE][gene];
				var actualDev = population[parent][STDEV][gene];

				var nextDev = actualDev * expRandnTau1 * Math.exp(this.standardRandomGaussianDeviation() * tau2);
				var nextGene = actualGene + this.standardRandomGaussianDeviation() * nextDev;

				genes.push(nextGene);
				stDev.push(nextDev);
			}

			nextGeneration.push([genes, stDev]);
		}

		return nextGeneration;
	}

	this.initializePopulation = function() {

		for (var j=0; j<POPULATION_NUMBER; j++) {
			var s = this.standardRandomGaussianDeviation();
			var xVector = new Array();
			var standardDeviationVector = new Array();

			for (var i=0; i<NUMBER_OF_GENES; i++) {
				xVector.push(s);
				standardDeviationVector.push(Math.abs(s));	
			}

			population.push([xVector, standardDeviationVector]);
		}
	}	
}